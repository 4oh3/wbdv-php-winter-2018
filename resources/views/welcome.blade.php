@extends('layout')

@section('content')
      <div class="navBar">
        <ul>
            <li><i class="fa fa-twitter" aria-hidden="true"></i>Home</li>
            <li>Moments</li>
            <li></li>
            <li><input placeholder="Search Twitter" type="text"></li>
            <li>Log In<i class="fa fa-caret-down" aria-hidden="true"></i></li>
        </ul>
      </div>

    <div class="">
      div2
    </div>
    <div class="">
      div3
    </div>

    <ul class="users">
      <li>
        <ul class="tweetsMenu">
          <li>
            <h2>Users</h2>
          </li>
        </ul>
      </li>
      <?php foreach ($users as $user): ?>
        <li>
          <img src="<?php echo $user->image ?>"> <br>
          <span class="name"><?php echo $user->name ?> <br></span>
          @<?php echo $user->handle ?>
        </li>
      <?php endforeach; ?>
    </ul>

    <ul class="tweets">
      <li>
        <ul class="tweetsMenu">
          <li>
            <h2><a href="#">Tweets</a></h2>
          </li>
          <li>
            <h2><a href="#">Tweets & replies</a></h2>
          </li>
          <li>
            <h2><a href="#">Media</a></h2>
          </li>
        </ul>
      </li>

      <?php foreach ($tweets as $tweet): ?>
        <li>
          <img src="<?php echo $tweet->user->image ?>"> <br>
          <span class="name"><?php echo $tweet->user->name ?></span>
          <span class="atname">@<?php echo $tweet->user->handle ?></span>
          <span class="time"><?php echo $tweet->time ?> <br></span>
          <?php echo $tweet->content ?> <br>
          <span class="tweetIcons">
          <?php echo count($tweet->comments) ?> <i class="fa fa-comment-o" aria-hidden="true"></i>
          <?php echo count($tweet->retweets) ?> <i class="fa fa-retweet" aria-hidden="true"></i>
          <?php echo count($tweet->likes) ?> <i class="fa fa-heart-o"></i>
          </span>
        </li>
      <?php endforeach; ?>

    </ul>
@endsection
