<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Twitter</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        @yield('content')
    </body>
</html>
