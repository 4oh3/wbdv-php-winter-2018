<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Faker\Factory;

class Tweet {
  public $user;
  public $time;
  public $content;
  public $comments = [];
  public $retweets = [];
  public $likes = [];
}

class User {
  public $name;
  public $handle;
  public $image;
  public $dateJoined;
}

Route::get('/about', function () {
    return view('about');
});

Route::get('/', function () {

    $faker = Factory::create();

    $user1 = new User();
    $user1->name = $faker->name;
    $user1->handle = 'realDonaldTrump';
    $user1->image = $faker->imageUrl($width = 75, $height = 75, 'people');
    $user1->dateJoined = date_create();

    $user2 = new User();
    $user2->name = $faker->name;
    $user2->handle = 'BarackObama';
    $user2->image = $faker->imageUrl($width = 75, $height = 75, 'people');
    $user2->dateJoined = date_create();

    $users = [$user1, $user2];

    $tweet1 = new Tweet();
    $tweet1->user = $user1;
    $tweet1->time = ' · ' . $faker->monthName($max = 'now') . ' ' . $faker->dayOfMonth($max = 'now');
    $tweet1->comments = [];
    $tweet1->retweets = [];
    $tweet1->likes = [];
    $tweet1->content = $faker->text($maxNbChars = 280);

    $tweet2 = new Tweet();
    $tweet2->user = $user2;
    $tweet2->time = ' · ' . $faker->monthName($max = 'now') . ' ' . $faker->dayOfMonth($max = 'now');
    $tweet2->comments = [];
    $tweet2->retweets = [];
    $tweet2->likes = [];
    $tweet2->content = $faker->text($maxNbChars = 280);

    $tweets = [$tweet1, $tweet2];

    $data = [
      'users' => [$user1, $user2],
      'tweets' => [$tweet1, $tweet2]
    ];

    return view('welcome', $data);
});
